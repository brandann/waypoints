﻿using UnityEngine;
using System.Collections;

public class WaypointUIControls : MonoBehaviour {
	
	public void Button_StartRandom()
	{
		var PointList = GetAllWaypoints();
		var HeroList = GameObject.FindGameObjectsWithTag("Hero");
		foreach (GameObject go in HeroList)
		{
			var hero = go.GetComponent<TraverseWaypoints>();
			hero.SetPoints(
				PointList, 
				TraverseWaypoints.eWaypointAction.Random, 
				TraverseWaypoints.eWhereToStart.Current, 
				true
			);
		}
	}

	public void Button_StartTraverse()
	{
		var PointList = GetAllWaypoints();
		var HeroList = GameObject.FindGameObjectsWithTag("Hero");
		foreach (GameObject go in HeroList)
		{
			var hero = go.GetComponent<TraverseWaypoints>();
			hero.SetPoints(
				PointList,
				TraverseWaypoints.eWaypointAction.TraverseNext,
				TraverseWaypoints.eWhereToStart.Current,
				true
			);
		}
	}

	// EXAMPLE CODE ONLY TO DISPLAY THE TRAVERSE WAYPOINT FUNCTIONS
	private Vector3[] GetAllWaypoints()
	{
		var GOlist = GameObject.FindGameObjectsWithTag("Point");
		var WPlist = new Vector3[GOlist.Length];
		for(int i = 0; i < GOlist.Length; i++)
			WPlist[GOlist[i].gameObject.GetComponent<PointIndex>().Index - 1] = GOlist[i].transform.position;
		return WPlist;
	}
}
